# treeSelect.js

#### 项目介绍
对已有项目的二次修改，原项目地址：https://gitee.com/wujiawei0926/treeselect


#### 使用说明

1. 使用参考原项目，我只是修改新增部分功能
2. 修改比较模式，改成id比较是否选中
3. 对原项目的节点获取改成递归方式
4. 新增初始化完毕后回调：
![输入图片说明](https://images.gitee.com/uploads/images/2018/0910/102009_69ac9e9f_424424.png "屏幕截图.png")
